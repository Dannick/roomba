
#pragma once

#include <string>

enum class ConnectionStatus{NOT_CONNECTED, CONNECTED, ERROR};

class SerialConnection
{
private:
    int m_FileDescriptor;
    ConnectionStatus m_Status{ConnectionStatus::NOT_CONNECTED};

private:
    void PrintError();

public:
    SerialConnection();
    ~SerialConnection();

    void Open(const char *device, const unsigned int baudrate);
    void Close();
    void SendByte(const unsigned char data);
    void SendString(const char *string);
    int  CharactersAvailable();
    int  ReadByte();
};