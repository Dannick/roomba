
#include "PiSerial.h"
#include <iostream>
#include <unistd.h>
#include <string>

int main()
{
    std::string writeBuffer;  
    int readBuffer;
    
    const char *serialPort  = "/dev/serial0";
    int   baudrate;
    SerialConnection HC06;

    std::cout << "Enter baudrate > " << std::flush;
    std::cin >> baudrate;

    std::cout << "Opening connection..." << std::endl;
    HC06.Open(serialPort, baudrate); 

    sleep(1);

    while(1)    
    {              
        std::cout << "Enter command > " << std::flush;
        std::cin  >> writeBuffer;

        //writeBuffer += "\r\n";
        std::cout << "Sending command > " << writeBuffer << std::endl;

        HC06.SendString(writeBuffer.c_str());
        sleep(1);
        std::cout << "Response > " << std::flush;

        while (HC06.CharactersAvailable() > 0)
        { 
            readBuffer = HC06.ReadByte();
            std::cout << (unsigned char)readBuffer;
        }        

        std::cout << std::endl;
        sleep(1);
    }

    HC06.Close();

    return 0;
}
