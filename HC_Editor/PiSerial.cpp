
#include <wiringSerial.h>
#include <iostream>
#include <cerrno>
#include <cstring>

#include "PiSerial.h"

SerialConnection::SerialConnection()
{
}

SerialConnection::~SerialConnection()
{       
    Close();
}

void SerialConnection::Open(const char *device, const unsigned int baudrate)
{
    m_FileDescriptor = serialOpen(device, baudrate);
    m_Status = ConnectionStatus::CONNECTED;

    if (m_FileDescriptor == -1)
    {
        m_Status = ConnectionStatus::ERROR;
        PrintError();
    }
}

void SerialConnection::Close()
{    
    serialClose(m_FileDescriptor);
    m_Status = ConnectionStatus::NOT_CONNECTED;
}

void SerialConnection::SendByte(const unsigned char data)
{
    serialPutchar(m_FileDescriptor, data);
}

void SerialConnection::SendString(const char *string)
{
    serialPrintf(m_FileDescriptor, string);
}

int SerialConnection::CharactersAvailable()
{
    return serialDataAvail(m_FileDescriptor);
}

int SerialConnection::ReadByte()
{
    return serialGetchar(m_FileDescriptor);
}

void SerialConnection::PrintError()
{
    std::cout << "Serial connection ERROR" << std::endl;
    std::cout << std::strerror(errno) << std::endl;
}