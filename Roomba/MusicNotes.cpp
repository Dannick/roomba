#include "MusicNotes.h"
#include "Roomba.h"
#include "unistd.h"
#include "iostream"
#include "MusicNotes.h"
#include "Statemachine.h"
#include "Opcodes.h"

const unsigned int SongCapacity = 4;
unsigned int MaxCapacityOfNotes = 16;
unsigned int MaxDurationOfNote = 3.9;

/**
 * @brief First function to use when making music notes.
 * 
 * @param SongNumber is used to decide where to save you song.
 * @param NumberOfNotes is used to delcare how many notes you are going to use.
 */
void Roomba::CreateNotes(unsigned int SongNumber, unsigned int NumberOfNotes)
{
    if (SongNumber <= SongCapacity && NumberOfNotes <= MaxCapacityOfNotes)
    {
        SendOpcode(OPCODE::CREATE_NOTES);
        SendByte(SongNumber);
        SendByte(NumberOfNotes);
    }
    else
    {
        std::cout << ("ERROR: WRONG VALUE") << std::endl;
        std::cout << ("Please enter a value between 0 and 5 for the songnumber.") << std::endl;
        std::cout << ("Please enter a value between 0 and 17 for the notedurations.") << std::endl;
    }
}

/**
 * @brief Function to select to song you want to play on the Roomba.
 * 
 * @param SongNumber is used to select the songnumber you want to play on the Roomba.
 */
void Roomba::SelectSong(unsigned int SongNumber)
{
    if (SongNumber <= SongCapacity)
    {
        SendOpcode(OPCODE::SELECT_SONG);
        SendByte(SongNumber);
    }
    else
    {
        std::cout << ("ERROR: WRONG VALUE") << std::endl;
        std::cout << ("Please enter a value between 0 and 5 for the songnumber.") << std::endl;
    }
}

/**
 * @brief Function to tell how long you want your note to last.
 * 
 * @param TimeOfNote is used to determine the amount of seconds for your note to last.
 */
void Roomba::NoteDuration(float TimeOfNote)
{
    if (TimeOfNote <= MaxDurationOfNote)
    {
        SendByte(TimeOfNote * 64);
    }
    else
    {
        std::cout << ("ERROR: WRONG VALUE") << std::endl;
        std::cout << ("Please enter a value between 0 and 4.") << std::endl;
    }
}

/**
 * @brief Function to play the Darth vader theme of Star Wars.
 * 
 */
void Roomba::PlayDarthVader()
{
    CreateNotes(0, 9);

    SendByte(MusicNote_G);
    NoteDuration(0.5);

    SendByte(MusicNote_G);
    NoteDuration(0.5);

    SendByte(MusicNote_G);
    NoteDuration(0.5);

    SendByte(MusicNote_D__);
    NoteDuration(0.375);

    SendByte(MusicNote_A_);
    NoteDuration(0.25);

    SendByte(MusicNote_G);
    NoteDuration(0.5);

    SendByte(MusicNote_D__);
    NoteDuration(0.5);

    SendByte(MusicNote_A_);
    NoteDuration(0.25);

    SendByte(MusicNote_G);
    NoteDuration(0.75);

    //---------------------------------------------------------------------------
    CreateNotes(1, 9);

    SendByte(MusicNote_D);
    NoteDuration(0.5);

    SendByte(MusicNote_D);
    NoteDuration(0.5);

    SendByte(MusicNote_D);
    NoteDuration(0.5);

    SendByte(MusicNote_D_);
    NoteDuration(0.5);

    SendByte(MusicNote_A_);
    NoteDuration(0.25);

    SendByte(MusicNote_F_);
    NoteDuration(0.5);

    SendByte(MusicNote_D__);
    NoteDuration(0.5);

    SendByte(MusicNote_A_);
    NoteDuration(0.25);

    SendByte(MusicNote_G);
    NoteDuration(1);

    SelectSong(0);
    sleep(5);

    SelectSong(1);
    sleep(5);
}