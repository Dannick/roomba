
#include "Gamepad.h"
#include <iostream>

void Gamepad::Init(const char *device)
{
    m_Device = (char *)device;
    m_FileDescriptor = open(m_Device, O_RDONLY);
    
    if (m_FileDescriptor == -1)
        m_Plugged = false;
    else    
    {
        m_Plugged = true;
        m_GamepadUpdateThread = std::thread(&Gamepad::ReadGamepadInputThread, this);
    }
}

void Gamepad::Close()
{
    if (m_Plugged = true)
    {
        m_GamepadUpdateThread.join();

        close(m_FileDescriptor);
        m_Plugged = false;
    }
}

void Gamepad::ReadGamepadInputThread()
{
    while(!m_Shutdown)
    {
        if (EventAvailable())
            ProcesEvent();
    }

    exit(1);
}

bool Gamepad::EventAvailable()
{
    ssize_t bytes;
    bytes = read(m_FileDescriptor, &m_Event, sizeof(m_Event));

    if (bytes == sizeof(m_Event)) 
        return true;

    return false;
}

void Gamepad::ProcesEvent()
{
    switch(m_Event.type)
    {
        case JS_EVENT_BUTTON:
            UpdateButtonInput();
            break;

        case JS_EVENT_AXIS:
            UpdateStickInput();
            break;

        default:
            break;
    }    
}

StickPosition Gamepad::GetStickPosition(int axis)
{
    return m_StickAxis[axis];
}

bool Gamepad::GetButton(BUTTON button)
{
    return m_Buttons[(int)button];
}

void Gamepad::PrintGamepadInput()
{
    StickPosition axis[2];
    bool *buttons;

    m_GamepadUpdateMutex.lock();
    axis[0] = m_StickAxis[0];
    axis[1] = m_StickAxis[1];
    buttons = m_Buttons;
    m_GamepadUpdateMutex.unlock();

    std::cout << "Left stick  " << "[" << axis[0].xAxis << "][" << axis[0].yAxis << "]\n";
    std::cout << "Right stick " << "[" << axis[1].xAxis << "][" << axis[1].yAxis << "]\n";
    std::cout << "Buttons =   ";

    for (int i = 0; i < 16; i ++)
    {
        std::cout << buttons[i];    
    }

    std::cout << std::endl;}

void Gamepad::PrintGamepadStatus()
{
    if (m_Plugged = false)
        std::cout << "ERROR connecting joystick..." << std::endl;
    else
        std::cout << "Joystick succesfully connected!" << std::endl;
}

bool Gamepad::IfPlugged()
{
    return m_Plugged;
}

void Gamepad::Unplug()
{
    m_Shutdown = true;
}

int Gamepad::ProcesDeadzone(int position)
{
    int newPosition = 0;

    if (position >=  m_Deadzone) newPosition = position - m_Deadzone;
    if (position <= -m_Deadzone) newPosition = position + m_Deadzone;

    newPosition *= m_RangeScaling;

    return newPosition;
}