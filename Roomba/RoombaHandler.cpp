#include "RoombaHandler.h"
#include "Gamepad.h"
#include <algorithm>

RoombaHandler::RoombaHandler() {
    systemPrint("Creating handler class");
    roombaTask = T_INITIALISE;
    enableUserInput = false;
}

RoombaHandler::~RoombaHandler() {
    std::cout << "Destructor called" << std::endl;
}

std::string RoombaHandler::askInput() {
    systemPrint("Enter a command please:");
    std::string input;
    std::cin >> input;
    return input;
}

void RoombaHandler::handler() {
    if(enableUserInput) { roombaTask = task_e(checkingInput()); }
    if(roombaTask == T_DRIVE_SLOW){ roomba.DriveSlow(); roombaPrint("Driving Slow"); }
    if(roombaTask == T_DRIVE_NORMAL)     { roomba.DriveNormal(); roombaPrint("Driving Normal"); }
    if(roombaTask == T_DRIVE_FAST)       { roomba.DriveFast(); roombaPrint("Driving Fast"); }
    if(roombaTask == T_LEFT)             { roomba.Left(); roombaPrint("Turning left"); }
    if(roombaTask == T_RIGHT)            { roomba.Right(); roombaPrint("Turning right"); }
    if(roombaTask == T_TEST_LED)         { roomba.TestLED(); roombaPrint("Test LED activated"); }
    if(roombaTask == T_IDLE)             { roomba.Idle(); roombaPrint("Idling.."); }
    if(roombaTask == T_ENABLE_SAFEMODE)  { roomba.SendOpcode(OPCODE::SAFEMODE); roombaPrint("Enabling Safemode.."); }
    if(roombaTask == T_ENABLE_FULLMODE)  { roomba.SendOpcode(OPCODE::FULLMODE); roombaPrint("Enabling Fullmode.."); }
    if(roombaTask == T_SEEK_DOCK)        { roomba.SendOpcode(OPCODE::SEEK_DOCKING_STATION); roombaPrint("Enabling Safemode.. Seeking Dock.."); }
    if(roombaTask == T_POWER_OFF)        { roomba.SendOpcode(OPCODE::POWEROFF); roombaPrint("Shutting down.."); exit(0); }
    if(roombaTask == T_REMOTE_CONTROL)   { RemoteControl(); }
    if(roombaTask == T_DARTH_VADER)      { roomba.PlayDarthVader(); roombaPrint("Playing Darth Vader theme");}
    if(roombaTask == T_INITIALISE)       { Initialise(); }
    if(roombaTask == T_INFO)             { printingOptions();}
    if(roombaTask == T_ERROR)            { errorPrint("Received wrong input"); printingOptions();}
}

void RoombaHandler::RemoteControl() {
    roombaPrint("Started Remote control");
    enableUserInput = false;

    leftStick  = gamepad.GetStickPosition(0);
    rightStick = gamepad.GetStickPosition(1);

    roombaVelocity  = leftStick.yAxis  * MAX_FORWARD_PWM / MAX_STICK_VALUE;
    roombaTurnAngle = rightStick.xAxis * MAX_FORWARD_PWM / MAX_STICK_VALUE;

    roomba.DrivePWM(roombaVelocity, roombaTurnAngle);

    usleep(REMOTECONTROL_REFRESH_RATE);

      if (gamepad.GetButton(BUTTON::LEFT_BUMPER))
         roomba.EnableVacuum();
      else
         roomba.DisableVacuum();

      if (gamepad.GetButton(BUTTON::BACK))
      {
          enableUserInput = true;
          roomba.SendOpcode(OPCODE::SAFEMODE);
          systemPrint("Exiting remote control...");
      }    

}

void RoombaHandler::Initialise() {
    roomba.SendOpcode(OPCODE::ENABLE_OI); 
    roomba.SendOpcode(OPCODE::SAFEMODE);
    enableUserInput = true;
    systemPrint("Initialising Roomba..");
}


int RoombaHandler::checkingInput()
{     
    std::string input;
    std::string compareString[T_ASIGNABLE_TASK];
    compareString[T_DRIVE_SLOW] = "driveslow"; 
    compareString[T_DRIVE_NORMAL] = "drivenormal";
    compareString[T_DRIVE_FAST] = "drivefast";
    compareString[T_LEFT] = "left";
    compareString[T_RIGHT] = "right";
    compareString[T_TEST_LED] = "test";
    compareString[T_IDLE] = "idle";
    compareString[T_ENABLE_SAFEMODE] = "safe";
    compareString[T_ENABLE_FULLMODE] = "full";
    compareString[T_SEEK_DOCK] = "dock";
    compareString[T_POWER_OFF] = "poweroff";
    compareString[T_REMOTE_CONTROL] = "controller";
    compareString[T_DARTH_VADER] = "vader";
    compareString[T_INFO] = "help";

    input = askInput();

    for(int i = 0; i < T_ASIGNABLE_TASK; i++)
    {
        if(compareString[i].compare(input) == 0)
        {
            return i;
        }
    }
    return T_ERROR;
}

void RoombaHandler::printingOptions()
{
    systemPrint("===================================HELP===================================");
    systemPrint("command: 'driveslow' = Roomba will slowly move forward."                   );
    systemPrint("command: 'drivenormal' = Roomba will move forward."                        );
    systemPrint("command: 'drivefast' =  Roomba will move forwards faster."                 );
    systemPrint("command: 'left' = Roomba will turn left."                                  );
    systemPrint("command: 'right' = Roomba wll turn right."                                 );
    systemPrint("command: 'test' = A testing LED will be activated."                        );
    systemPrint("command: 'idle' = Roomba will stop and idle."                              );
    systemPrint("command: 'safe' = Roomba will switch to savemode."                         );
    systemPrint("command: 'full' = Roomba will switch to fullmode."                         );
    systemPrint("command: 'dock' = Roomba will automaticly look for the dockstation."       );
    systemPrint("command: 'poweroff' = Roomba will shutdown."                               );
    systemPrint("command: 'controller' = Roomba can now be controlled by Xbox controller."  );
    systemPrint("command: 'vader' = Roomba will play the Darth Vader theme song."           );
    systemPrint("==========================================================================");
}

void RoombaHandler::roombaPrint(std::string text)
{
    if(enableUserInput) {
        std::cout << ROOMBACOLOR << text << CLEARCOLOR << std::endl;
    }
}

void RoombaHandler::errorPrint(std::string text)
{
    if(enableUserInput) {
        std::cout << ERRORCOLOR << text << CLEARCOLOR << std::endl;
    }
}

void RoombaHandler::systemPrint(std::string text)
{
    if(enableUserInput) {
        std::cout << SYSTEMCOLOR << text << CLEARCOLOR << std::endl;
    }
}