
#include "Roomba.h"

/**
 * @brief Opens a serial connection with the roomba
 * 
 * @param device location of the serial device in Linux
 * @param baudrate baudrate of the connection
 */

void Roomba::Connect(const char *device, const unsigned int baudrate)
{
    serialConnection.Open(device, baudrate);
}

/**
 * @brief Closes the serial connection with the roomba
 * 
 */

void Roomba::Disconnect()
{
    serialConnection.Close();
}

/**
 * @brief Sends a remote control drive command
 * 
 * @param velocity speed of the roomba, min = -500 and max = 500
 * @param turnAngle radius of how the roomba will turn, a bigger radius means straighter line
 */

void Roomba::Drive(int velocity, int turnAngle)
{
    SetVelocity(velocity);
    SetTurnAngle(turnAngle);
    SendDriveCommand();
}

/**
 * @brief converts a 16 bit signed integer into 2 bytes
 * 
 * @param value 16 bit signed integer
 * @return DataPacket, contains highByte and lowByte
 */

DataPacket Roomba::ConvertToDataPacket(int value)
{
    DataPacket dataPacket;
    int mask = 0b11111111;

    dataPacket.value    = value;
    dataPacket.highByte = ((value >> 8) & mask);
    dataPacket.lowByte  = ((value >> 0) & mask);

    return dataPacket;
}

/**
 * @brief loads given velocity into class struct
 * 
 * @param velocity 
 */

void Roomba::SetVelocity(int velocity)
{
    m_Velocity = ConvertToDataPacket(velocity);
}

/**
 * @brief loads given turn radius into class struct
 * 
 * @param turnAngle 
 */

void Roomba::SetTurnAngle(int turnAngle)
{
    m_TurnAngle = ConvertToDataPacket(turnAngle);
}

/**
 * @brief transmits the drive opcode, velocity and turn radius to the roomba
 * 
 */

void Roomba::SendDriveCommand()
{
    SendOpcode(OPCODE::DRIVE);
    serialConnection.SendByte(m_Velocity.highByte);
    serialConnection.SendByte(m_Velocity.lowByte);
    serialConnection.SendByte(m_TurnAngle.highByte);
    serialConnection.SendByte(m_TurnAngle.lowByte);
}

/**
 * @brief turns given velocity and turn angle into PWM values for the roomba, sends command afterwards
 * 
 * @param velocity requested velocity of the roomba, min = -32768 max = 32767
 * @param turnAngle higher values means faster turns, min = -32768 max = 32767
 */

void Roomba::DrivePWM(int velocity, int turnAngle)
{
    int rightWheelPWM = velocity - turnAngle * 2/3;
    int leftWheelPWM  = velocity + turnAngle * 2/3;

    rightWheelPWM = ClipDrivePWM(rightWheelPWM);
    leftWheelPWM  = ClipDrivePWM(leftWheelPWM);

    SetDrivePWM(rightWheelPWM, leftWheelPWM);
    SendDrivePWMCommand();
}

/**
 * @brief clips value between -255 and 255
 * 
 * @param value
 * @return int 
 */

int Roomba::ClipDrivePWM(int value)
{
    int newValue = value;

    if (newValue > MAX_FORWARD_PWM) newValue = MAX_FORWARD_PWM;
    if (newValue < MAX_REVERSE_PWM) newValue = MAX_REVERSE_PWM;

    return newValue;
}

/**
 * @brief loads right and left wheel PWM values into class variables
 * 
 * @param rightWheel 
 * @param leftWheel 
 */

void Roomba::SetDrivePWM(int rightWheel, int leftWheel)
{    
    m_RightWheelPWM = ConvertToDataPacket(rightWheel);
    m_LeftWheelPWM  = ConvertToDataPacket(leftWheel);    
}

/**
 * @brief transmits drivePWM opcode, left and right wheel values to the roomba
 * 
 */

void Roomba::SendDrivePWMCommand()
{
    SendOpcode(OPCODE::DRIVE_PWM);
    serialConnection.SendByte(m_RightWheelPWM.highByte);
    serialConnection.SendByte(m_RightWheelPWM.lowByte);
    serialConnection.SendByte(m_LeftWheelPWM.highByte);
    serialConnection.SendByte(m_LeftWheelPWM.lowByte);
}

/**
 * @brief enables roomba vacuum
 * 
 */

void Roomba::EnableVacuum()
{
    SendOpcode(OPCODE::MOTOR);
    SendByte(ENABLE_VACUUM);
}

/**
 * @brief disables roomba vacuum
 * 
 */

void Roomba::DisableVacuum()
{
    SendOpcode(OPCODE::MOTOR);
    SendByte(DISABLE_VACUUM);
}

/**
 * @brief transmits given opcode to the roomba
 * 
 * @param opcode 
 */

void Roomba::SendOpcode(OPCODE opcode)
{
    serialConnection.SendByte((int)opcode);
}

/**
 * @brief transmits given data byte to the roomba
 * 
 * @param byte 
 */

void Roomba::SendByte(unsigned char byte)
{
    serialConnection.SendByte(byte);
}

/**
 * @brief drives forward with 1/4 of the max speed
 * 
 */

void Roomba::DriveSlow()
{
    Drive(MAX_FORWARD_SPEED / 4, STRAIGHT);
}

/**
 * @brief drives forward with 1/2 of the max speed
 * 
 */

void Roomba::DriveNormal()
{
    Drive(MAX_FORWARD_SPEED / 2, STRAIGHT);
}

/**
 * @brief drives forward with max speed
 * 
 */

void Roomba::DriveFast()
{
    Drive(MAX_FORWARD_SPEED, STRAIGHT);
}

/**
 * @brief drives the roomba while turning left
 * 
 */

void Roomba::Left()
{
    Drive(MAX_FORWARD_SPEED / 3, MAX_LEFT_TURN);
}

/**
 * @brief drives the roomba while turning right
 * 
 */

void Roomba::Right()
{
    Drive(MAX_FORWARD_SPEED / 3, MAX_RIGHT_TURN);
}

/**
 * @brief stops the roomba
 * 
 */

void Roomba::Idle()
{
    Drive(0, 0);
}

/**
 * @brief turns on a led
 * 
 */

void Roomba::TestLED()
{
    SendOpcode(OPCODE::LED);
    serialConnection.SendByte(ADVANCEDLED);
    serialConnection.SendByte(LEDCOLOR);
    serialConnection.SendByte(LEDINTENSITY);
}
