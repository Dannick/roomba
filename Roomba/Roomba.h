
#pragma once

#include "PiSerial.h"
#include "Opcodes.h"
#include <unistd.h>

#define MAX_FORWARD_SPEED 500
#define MAX_REVERSE_SPEED -500
#define MAX_FORWARD_PWM 255
#define MAX_REVERSE_PWM -255

#define ENABLE_VACUUM 7
#define DISABLE_VACUUM 0

typedef struct
{
    int value;
    int highByte;
    int lowByte;
} DataPacket;

class Roomba
{
private:
    DataPacket m_Velocity;
    DataPacket m_TurnAngle;
    DataPacket m_LeftWheelPWM;
    DataPacket m_RightWheelPWM;

    const int MAX_LEFT_TURN  = 0x0001;
    const int MAX_RIGHT_TURN = 0xFFFF;
    const int STRAIGHT = 0x8000;
    
    const int ADVANCEDLED = 8;
    const int LEDCOLOR = 255;
    const int LEDINTENSITY = 255;

    SerialConnection serialConnection{"/dev/serial0", 9600};

private:
    DataPacket ConvertToDataPacket(int value);
    void SetVelocity(int velocity);
    void SetTurnAngle(int turnAngle);

public:
    Roomba(const char *device, const unsigned int baudrate)
    {
        Connect(device, baudrate);
    }

    Roomba()
    {
        Connect("/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A50285BI-if00-port0", 115200);    
    }

    ~Roomba()
    {   
        Disconnect();
    }

    void SendOpcode(OPCODE opcode);
    void SendByte(unsigned char byte);
    void Connect(const char *device, const unsigned int baudrate);
    void Disconnect();
    void Drive(int velocity, int turnangle);
    void DrivePWM(int velocity, int turnAngle);
    int  ClipDrivePWM(int value);
    void SetDrivePWM(int rightWheel, int leftWheel);
    void SendDriveCommand();
    void SendDrivePWMCommand();
    void EnableVacuum();
    void DisableVacuum();

    void DriveSlow();
    void DriveNormal();
    void DriveFast();
    void Left();
    void Right();
    void TestLED();
    void Idle();
    void RotateClockwise();
    void RotateCounterClockwise();
    void CreateNotes(unsigned int SongNumber, unsigned int NumberOfNotes);
    void SelectSong(unsigned int SongNumber);
    void PlayDarthVader();
    void NoteDuration(float TimeOfNote);
};
