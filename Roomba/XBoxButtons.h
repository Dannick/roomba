
#pragma once

enum class BUTTON
{
    A = 0,
    B = 1,
    X = 2,
    Y = 3,
    RIGHT_BUMPER = 4,
    LEFT_BUMPER = 5,
    BACK = 6,
    START = 7,
    HOME = 8,
    LEFT_STICK = 9,
    RIGHT_STICK = 10
};
