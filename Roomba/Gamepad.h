
#pragma once

#include <linux/joystick.h>
#include <unistd.h>
#include <fcntl.h>
#include <thread>
#include <mutex>
#include "XBoxButtons.h"

/**
 * @brief struct which holds the high and low bytes of a 16 bit integer
 * 
 */

typedef struct
{
    int xAxis;
    int yAxis;
    int trigger;
} StickPosition;

/**
 * @brief parent class of controllers
 * 
 */

class Gamepad
{
public:
    Gamepad(){};
    virtual ~Gamepad(){};

    /**
     * @brief returns if the joystick is succesfully connected
     * 
     * @return true 
     * @return false 
     */

    bool IfPlugged();

    /**
     * @brief prints all joystick values to stdout
     * 
     */

    void PrintGamepadInput();

    /**
     * @brief prints current joystick status
     * 
     */

    void PrintGamepadStatus();

    /**
     * @brief returns a joystick stick value
     * 
     * @param axis 
     * @return StickPosition 
     */

    StickPosition GetStickPosition(int axis);

    /**
     * @brief returns button state
     * 
     * @param button 
     * @return true 
     * @return false 
     */

    bool GetButton(BUTTON button);

    /**
     * @brief disconnect from controller driver
     * 
     */

    void Unplug();

protected:
    bool  m_FlipAxisX   = false; /*!< if true the X axis values will be fliped */
    bool  m_FlipAxisY   = false; /*!< if true the Y axis values will be fliped */
    bool  m_FlipTrigger = false; /*!< if true the trigger values will be fliped */
    int   m_Deadzone    = 8000;  /*!< value of which all axis values will be set to 0 */
    int   m_MaxStickValue = 32767; /*!< max value a stick axis van have, default is 32767 */
    float m_RangeScaling = (float)m_MaxStickValue / ((float)m_MaxStickValue - (float)m_Deadzone); /*!< value needed to correct non deadzone values */

    char *m_Device; /*!< path to linux controller device */
    int   m_FileDescriptor{-1}; /*!< file discriptor if controller device */
    bool  m_Plugged{false}; /*!< bool if succesfull init */
    bool  m_Shutdown{false}; /*!< signal to disconnect from controller device */
    bool  m_Buttons[255]{0}; /*!< array of bools, represent the status of buttons */

    std::thread  m_GamepadUpdateThread; /*!< update thread where controller events are handeld */
    std::mutex   m_GamepadUpdateMutex; /*!< mutex to protect controller values */

    StickPosition   m_StickAxis[8] = {0}; /*!< X and Y values of controller sticks */
    struct js_event m_Event; /*!< event struct */

protected:
    /**
     * @brief attempts a connection to the joystick, opens read thread when succeded
     * 
     * @param device Linux joystick device
     */

    void Init(const char *device); 

    /**
     * @brief closes joystick filediscriptor and update thread
     * 
     */   

    void Close();

    /**
     * @brief joystick update thread, reads joystick events and blocks when non are present
     * 
     */

    void ReadGamepadInputThread();

    /**
     * @brief checks if a joystick event is triggert
     * 
     * @return true 
     * @return false 
     */

    bool EventAvailable();

    /**
     * @brief processes current joystick event
     * 
     */

    void ProcesEvent();

    /**
     * @brief adds a deadzone in the stick position values, everything in the deadzone musst be set to 0
     * 
     * @param position 
     * @return int 
     */

    int  ProcesDeadzone(int position);

    /**
     * @brief virtual function which saves the button statusus
     * 
     */

    virtual void UpdateButtonInput() = 0;

    /**
     * @brief virtual function which saves the stick axis and trigger statusus
     * 
     */

    virtual void UpdateStickInput() = 0;
};