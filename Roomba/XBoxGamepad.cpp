
#include "XBoxGamepad.h"
#include <iostream>

void XBoxGamepad::UpdateButtonInput()
{
    m_GamepadUpdateMutex.lock();
    m_Buttons[m_Event.number] =  m_Event.value ? true : false;
    m_GamepadUpdateMutex.unlock();
}

void XBoxGamepad::UpdateStickInput()
{
    // Stick axis are stored in three 16 bit signed integers, [X axis][Y axis][trigger]
    // for Xbox controller, 0-2 are is for the left side and 3-5 for the right one
    int stickIndex      = m_Event.number / 3;
    int stickPosition   = ProcesDeadzone(m_Event.value);

    m_GamepadUpdateMutex.lock();

    switch (m_Event.number % 3)
    {
    case 0:
        m_StickAxis[stickIndex].xAxis = stickPosition;
        if (m_FlipAxisX) m_StickAxis[stickIndex].xAxis = -stickPosition;
        break;

    case 1:
        m_StickAxis[stickIndex].yAxis = stickPosition;
        if (m_FlipAxisY) m_StickAxis[stickIndex].yAxis = -stickPosition;
        break;

    case 2:
        m_StickAxis[stickIndex].trigger = stickPosition;
        if (m_FlipTrigger) m_StickAxis[stickIndex].trigger = -stickPosition;
        break;

    default:
        break;
     }
    
    m_GamepadUpdateMutex.unlock();
}
