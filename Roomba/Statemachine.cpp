
#include "Statemachine.h"
#include "XBoxGamepad.h"

void Statemachine::handleEvent(event_e eventIn)
{
   // Handle sequential states.
   while (eventIn != E_NO)
   {
      eventIn = statemachine(eventIn);
   }
}

event_e Statemachine::statemachine(event_e eventIn)
{
   state_e nextState = S_NO;
   event_e eventOut = E_SEQ;

   int roombaVelocity = 0;
   int roombaTurnAngle = 0;

   // Sequential states must sent E_SEQ (= eventOut).
   // Sequential stepping ends with sending E_NO.
   switch (currentState)
   {
   case S_START:
      systemPrint("Starting up Roomba...");
      systemPrint("Initializing Roomba...");

      roomba.SendOpcode(OPCODE::ENABLE_OI);
      roomba.SendOpcode(OPCODE::SAFEMODE);

      nextState = S_INIT;
      eventOut = E_SEQ;
      break;

   case S_INIT:
      roombaPrint("Enter command please: ");
      
      nextState = S_WAIT_FOR_COMMAND;
      eventOut = E_NO; // Go waiting for external events: event driven
      break;

   case S_WAIT_FOR_COMMAND:
      switch (eventIn)
      {
      case E_INDRIVE_SLOW:
         nextState = S_DRIVE_SLOW;
         break;
      case E_INDRIVE_NORMAL:
         nextState = S_DRIVE_NORMAL;
         break;
      case E_INDRIVE_FAST:
         nextState = S_DRIVE_FAST;
         break;
      case E_INLEFT:
         nextState = S_LEFT;
         break;
      case E_INRIGHT:
         nextState = S_RIGHT;
         break;
      case E_INTEST_LED:
         nextState = S_TEST_LED;
         break;
      case E_INIDLE:
         nextState = S_IDLE;
         break;
      case E_INROTATE_CLOCKWISE:
         nextState = S_ROTATE_CLOCKWISE;
         break;
      case E_INROTATE_COUNTERCLOCKWISE:
         nextState = S_ROTATE_COUNTERCLOCKWISE;
         break;
      case E_INENABLE_SAFEMODE:
         nextState = S_ENABLE_SAFEMODE;
         break;
      case E_INENABLE_FULLMODE:
         nextState = S_ENABLE_FULLMODE;
         break;
      case E_INSEEK_DOCK:
         nextState = S_SEEK_DOCK;
         break;
      case E_INPOWER_OFF:
         nextState = S_POWER_OFF;
         break;
      case E_INREMOTE_CONTROL:
         nextState = S_REMOTE_CONTROL_SETUP;
         break; 
      case E_INFO:
         nextState = S_INFO;
         break;
      
      default:
         errorPrint("S_WAIT_FOR_COMMAND System ERROR: Unknown even");
         nextState = S_INFO;
      }
      eventOut = E_SEQ;
      break;

   case S_DRIVE_SLOW:
      roombaPrint("Driving Slow");
      roomba.DriveSlow();
      nextState = S_INIT;
      eventOut = E_SEQ;
      break;

   case S_DRIVE_NORMAL:
      roombaPrint("Drive Normal");
      roomba.DriveNormal();
      nextState = S_INIT;
      eventOut = E_SEQ;
      break;

   case S_DRIVE_FAST:
      roombaPrint("Driving Fast");
      roomba.DriveFast();
      nextState = S_INIT;
      eventOut = E_SEQ;
      break;

   case S_LEFT:
      roombaPrint("Turning left.");
      roomba.Left();
      nextState = S_INIT;
      eventOut = E_SEQ;
      break;

   case S_RIGHT:
      roombaPrint("Turning right");
      roomba.Right();
      nextState = S_INIT;
      eventOut = E_SEQ;
      break;

   case S_TEST_LED:
      roombaPrint("Test LED activated");
      roomba.TestLED();
      nextState = S_INIT;
      eventOut = E_SEQ;
      break;

   case S_IDLE:
      roombaPrint("Idling..");
      roomba.Idle();
      nextState = S_INIT;
      eventOut = E_SEQ;
      break;

   case S_ENABLE_SAFEMODE:
      roombaPrint("Enabling Safemode..");
      roomba.SendOpcode(OPCODE::SAFEMODE);
      nextState = S_INIT;
      eventOut = E_SEQ;
      break;

   case S_ENABLE_FULLMODE:
      roombaPrint("Enabling Fullmode..");
      roomba.SendOpcode(OPCODE::FULLMODE);
      nextState = S_INIT;
      eventOut = E_SEQ;
      break;

   case S_SEEK_DOCK:
      roombaPrint("Seeking Dock..");
      roomba.SendOpcode(OPCODE::SEEK_DOCKING_STATION);
      nextState = S_INIT;
      eventOut = E_SEQ;
      break;

   case S_POWER_OFF:
      roombaPrint("Shutting down..");
      roomba.SendOpcode(OPCODE::POWEROFF);
      exit(0);
   
   case S_REMOTE_CONTROL_SETUP:
      systemPrint("Enabling remote control...");

      roomba.SendOpcode(OPCODE::FULLMODE);

      nextState = S_REMOTE_CONTROL;
      eventOut = E_SEQ;
      break;


   case S_REMOTE_CONTROL:
      nextState = S_REMOTE_CONTROL;

      leftStick  = gamepad.GetStickPosition(0);
      rightStick = gamepad.GetStickPosition(1);

      roombaVelocity  = leftStick.xAxis  * MAX_FORWARD_PWM / MAX_STICK_VALUE;
      roombaTurnAngle = rightStick.yAxis * MAX_FORWARD_PWM / MAX_STICK_VALUE;

      roomba.DrivePWM(roombaVelocity, roombaTurnAngle);

      usleep(REMOTECONTROL_REFRESH_RATE);

      eventOut = E_SEQ;

      if (gamepad.GetButton(BUTTON::LEFT_BUMPER))
         roomba.EnableVacuum();
      else
         roomba.DisableVacuum();

      if (gamepad.GetButton(BUTTON::BACK))
      {
         nextState = S_REMOTE_CONTROL_EXIT;
         eventOut = E_SEQ;
      }    

      break;

   case S_REMOTE_CONTROL_EXIT:
      systemPrint("Exiting remote control...");
      
      roomba.SendOpcode(OPCODE::SAFEMODE);

      nextState = S_INIT;
      eventOut = E_SEQ;
      break;


   case S_INFO:
      printingOptions();
      nextState = S_INIT;
      eventOut = E_SEQ;
      break;

   default:
      errorPrint("Statemachine System ERROR: Unknown stat");
      eventOut = E_NO;
      nextState = S_INIT;
      break;
   }
   currentState = nextState;
   return eventOut;
}

int Statemachine::checkingInput()
{      
    std::string input;
    std::string compareString[15];
    compareString[E_INDRIVE_SLOW] = "driveslow"; 
    compareString[E_INDRIVE_NORMAL] = "drivenormal";
    compareString[E_INDRIVE_FAST] = "drivefast";
    compareString[E_INLEFT] = "left";
    compareString[E_INRIGHT] = "right";
    compareString[E_INTEST_LED] = "test";
    compareString[E_INIDLE] = "idle";
    compareString[E_INROTATE_CLOCKWISE] = "cw";
    compareString[E_INROTATE_COUNTERCLOCKWISE] = "ccw";
    compareString[E_INENABLE_SAFEMODE] = "safe";
    compareString[E_INENABLE_FULLMODE] = "full";
    compareString[E_INSEEK_DOCK] = "dock";
    compareString[E_INPOWER_OFF] = "poweroff";
    compareString[E_INREMOTE_CONTROL] = "controller";
    compareString[E_INFO] = "help";

    std::cin >> input;

    for(int i = 0; i < 15; i++)
    {
        if(compareString[i].compare(input) == 0)
        {
            return i;
        }
    }
    return -1;
}

void Statemachine::printingOptions()
{
   std::cout << "===================================HELP===================================" << std::endl;
   std::cout << "command: 'driveslow' = Roomba will slowly move forward."                    << std::endl;
   std::cout << "command: 'drivenormal' = Roomba will move forward."                         << std::endl;
   std::cout << "command: 'drivefast' =  Roomba will move forwards faster."                  << std::endl;
   std::cout << "command: 'left' = Roomba will turn left."                                   << std::endl;
   std::cout << "command: 'right' = Roomba wll turn right."                                  << std::endl;
   std::cout << "command: 'test' = A testing LED will be activated."                         << std::endl;
   std::cout << "command: 'idle' = Roomba will stop and idle."                               << std::endl;
   std::cout << "command: 'cw' = Roomba will turn on its position clockwise."                << std::endl;
   std::cout << "command: 'ccw' = Roomba will turn on its position counterclockwise."        << std::endl;
   std::cout << "command: 'safe' = Roomba will switch to savemode."                          << std::endl;
   std::cout << "command: 'full' = Roomba will switch to fullmode."                          << std::endl;
   std::cout << "command: 'dock' = Roomba will automaticly look for the dockstation."        << std::endl;
   std::cout << "command: 'poweroff' = Roomba will shutdown."                                << std::endl;
   std::cout << "command: 'controller' = Roomba can now be controlled by Xbox controller."   << std::endl;
   std::cout << "==========================================================================" << std::endl;
}

void Statemachine::roombaPrint(std::string text)
{
   std::cout << ROOMBACOLOR << text << CLEARCOLOR << std::endl;
}

void Statemachine::errorPrint(std::string text)
{
   std::cout << ERRORCOLOR << text << CLEARCOLOR << std::endl;
}

void Statemachine::systemPrint(std::string text)
{
   std::cout << SYSTEMCOLOR << text << CLEARCOLOR << std::endl;
}