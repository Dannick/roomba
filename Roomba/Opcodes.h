
#pragma once

enum class OPCODE{
            ENABLE_OI = 128,
            DISABLE_OI = 173,
            RESET_SETTINGS = 7,            
            DRIVE = 137,
            DRIVE_DIRECT = 145,
            DRIVE_PWM = 146,
            SAFEMODE = 131,
            FULLMODE = 132,
            SEEK_DOCKING_STATION = 143,
            CLEAN = 135,
            CLEAN_MAX = 136,
            CLEAN_SPOT = 134,
            POWEROFF = 133,
            LED = 139,
            MOTOR = 138,
            CREATE_NOTES = 140,
            SELECT_SONG = 141};