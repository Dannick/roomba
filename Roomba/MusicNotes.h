
#pragma once

#include "PiSerial.h"
#include "Opcodes.h"
#include <unistd.h>
#include "Roomba.h"

#define MusicNote_D__ 51
#define MusicNote_G 55
#define MusicNote_G_ 56
#define MusicNote_A 57
#define MusicNote_A_ 58
#define MusicNote_B 59
#define MusicNote_C 60
#define MusicNote_C_ 61
#define MusicNote_D 62
#define MusicNote_D_ 63
#define MusicNote_D__ 51
#define MusicNote_E 64
#define MusicNote_F 65
#define MusicNote_F_ 54
