#pragma once
#include "Roomba.h"
#include "XBoxGamepad.h"
#include <iostream>
#include <string>

#define CLEARCOLOR "\033[m"
#define SYSTEMCOLOR "\033[1;32m[SYSTEM] "
#define ERRORCOLOR "\033[1;31m[ERROR] "
#define ROOMBACOLOR "\033[1;34m[Roomba] "


typedef enum {    
    S_NO,
    S_DRIVE_SLOW,
    S_DRIVE_NORMAL, 
    S_DRIVE_FAST,
    S_LEFT, 
    S_RIGHT, 
    S_TEST_LED, 
    S_IDLE, 
    S_ROTATE_CLOCKWISE, 
    S_ROTATE_COUNTERCLOCKWISE, 
    S_ENABLE_SAFEMODE, 
    S_ENABLE_FULLMODE, 
    S_SEEK_DOCK, 
    S_POWER_OFF,
    S_REMOTE_CONTROL_SETUP,
    S_REMOTE_CONTROL,
    S_REMOTE_CONTROL_EXIT,
    S_INFO,
    S_START,
    S_INIT,
    S_WAIT_FOR_COMMAND
}state_e;

typedef enum {
    E_INDRIVE_SLOW, 
    E_INDRIVE_NORMAL, 
    E_INDRIVE_FAST,
    E_INLEFT, 
    E_INRIGHT, 
    E_INTEST_LED, 
    E_INIDLE, 
    E_INROTATE_CLOCKWISE, 
    E_INROTATE_COUNTERCLOCKWISE, 
    E_INENABLE_SAFEMODE, 
    E_INENABLE_FULLMODE, 
    E_INSEEK_DOCK, 
    E_INPOWER_OFF,
    E_INREMOTE_CONTROL,
    E_INFO,
    E_NO,
    E_SEQ,
    E_INIT,
    E_BACK_BUTTON
}event_e;


class Statemachine
{
public:
    Statemachine() { currentState = S_START; }
    ~Statemachine() { gamepad.Unplug(); };

    void handleEvent(event_e eventIn);
    state_e getCurrentState() const {return currentState; }

    int checkingInput();

    std::string input;

private:
    void printingOptions();
    void roombaPrint(std::string text);
    void errorPrint(std::string text);
    void systemPrint(std::string text);

private:
    Roomba roomba;
    XBoxGamepad gamepad{"/dev/input/js0"};

    StickPosition leftStick;
    StickPosition rightStick;
    const unsigned int REMOTECONTROL_REFRESH_RATE = 50 * 1000;

    state_e currentState;
    event_e statemachine(event_e eventIn);
};