
#pragma once

enum class ConnectionStatus{NOT_CONNECTED, CONNECTED, ERROR};

/**
 * @brief object which handels the serial connection
 * 
 */

class SerialConnection
{
private:
    int m_FileDescriptor;
    ConnectionStatus m_Status{ConnectionStatus::NOT_CONNECTED};

private:
    /**
     * @brief prints possible error
     * 
     */

    void PrintError();

public:
    SerialConnection(const char *device, const unsigned int baudrate)
    {
        Open(device, baudrate);
    }
    
    ~SerialConnection()
    {
        Close();
    }

    /**
     * @brief opens a serial connection with given device and baudrate
     * 
     * @param device 
     * @param baudrate 
     */

    void Open(const char *device, const unsigned int baudrate);

    /**
     * @brief Closes serial connection
     * 
     */

    void Close();

    /**
     * @brief transmits 1 byte of data over serial connection
     * 
     * @param data 
     */

    void SendByte(const unsigned char data);

    /**
     * @brief transmits an array of bytes over the serial connection
     * 
     * @param string 
     */

    void SendString(const char *string);

    /**
     * @brief returns total received bytes
     * 
     * @return int 
     */

    int  CharactersAvailable();

    /**
     * @brief reads a received byte, when no bytes are received the function will block
     * 
     * @return int 
     */

    int  ReadByte();
};