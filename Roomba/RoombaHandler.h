#pragma once
#include "Roomba.h"
#include "XBoxGamepad.h"
#include "MusicNotes.h"
#include <iostream>
#include <string>

#define CLEARCOLOR "\033[m"
#define SYSTEMCOLOR "\033[1;32m[SYSTEM] "
#define ERRORCOLOR "\033[1;31m[ERROR] "
#define ROOMBACOLOR "\033[1;34m[Roomba] "
/**
 * @brief Struct that contains all possible tasks for Roomba.
 * 
 */
typedef enum
{
    T_DRIVE_SLOW,
    T_DRIVE_NORMAL,
    T_DRIVE_FAST,
    T_LEFT,
    T_RIGHT,
    T_TEST_LED,
    T_IDLE,
    T_ENABLE_SAFEMODE,
    T_ENABLE_FULLMODE,
    T_SEEK_DOCK,
    T_POWER_OFF,
    T_REMOTE_CONTROL,
    T_DARTH_VADER,
    T_INFO,
    T_ASIGNABLE_TASK,
    T_ERROR,
    T_INITIALISE
} task_e;

/**
 * @brief Class containing handler for controlling the Roomba.
 * 
 */
class RoombaHandler
{
public:
/**
 * @brief Construct a new RoombaHandler object
 * 
 */
    RoombaHandler();
/**
 * @brief Destroy the RoombaHandler object
 * 
 */
    ~RoombaHandler();
/**
 * @brief Roomba handler function
 * 
 */
    void handler();

private:
/**
 * @brief Prints all availible commands on screen
 * 
 */
    void printingOptions();
/**
 * @brief Prints text on screen using format [Roomba] for clarity.
 * 
 * @param text 
 */
    void roombaPrint(std::string text);
/**
 * @brief Prints text on screen using format [Error] for clarity.
 * 
 * @param text 
 */
    void errorPrint(std::string text);
/**
 * @brief Prints text on screen using format [System] for clarity.
 * 
 * @param text 
 */
    void systemPrint(std::string text);
/**
 * @brief Function for navigating the roomba using (xbox)controller.
 * 
 */
    void RemoteControl();
/**
 * @brief Initialise function for handler
 * 
 */
    void Initialise();
/**
 * @brief Prints message to ask for user input and returns it.
 * 
 * @return std::string 
 */
    std::string askInput();
/**
 * @brief Compares input with known options. If correct returns the task. If incorrect returns error.
 * 
 * @return int 
 */
    int checkingInput();

    bool enableUserInput; /** disables user input and printing when false */

    task_e roombaTask;
    Roomba roomba; /** Roomba object, needed to control the roomba */
    XBoxGamepad gamepad{"/dev/input/js0"}; /** Controller object, needed to read input from the controller */

    StickPosition leftStick; /** X and Y axis information of the left stick */
    StickPosition rightStick; /** X and Y axis information of the right stick */

    int roombaVelocity = 0; /** total desired forward momentum of the roomba */
    int roombaTurnAngle = 0; /** angle of which the roomba will turn while driving */
    const unsigned int REMOTECONTROL_REFRESH_RATE = 50 * 1000; /** angle of which the roomba will turn while driving */
};