
#pragma once

#include "Gamepad.h"

#define MAX_STICK_VALUE 32767

/**
 * @brief derived class of Gamepad, handels the XBox360 controller input
 * 
 */

class XBoxGamepad: public Gamepad
{
public:
    XBoxGamepad(const char *device)
    { 
        m_Deadzone      = 8000;
        m_FlipAxisX     = false;
        m_FlipAxisY     = true;
        m_FlipTrigger   = false;
        m_RangeScaling = (float)m_MaxStickValue / ((float)m_MaxStickValue - (float)m_Deadzone);

        Init(device);
        PrintGamepadStatus();
    }

    ~XBoxGamepad()
    {
        Close();
    }

private:
    /**
     * @brief locks mutex, writes button data to m_Button array and unlocks mutex aferwards
     * 
     */

    void UpdateButtonInput();

    /**
     * @brief locks mutex, writes joystick stick data to m_StickAxis and unlocks mutex aferwards
     * 
     */

    void UpdateStickInput();
};